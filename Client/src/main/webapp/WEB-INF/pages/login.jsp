<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<script>
$(document).ready(function(){
	$(function () {
	    $('.tlt').textillate();
	});
});

function checkInfo() {
    var ok = true;

    var username = document.getElementById("username");
    var password = document.getElementById("password");

    if (username.value == "") {
        username.style.backgroundColor = "orange";
        ok = false;
    } else
        username.style.backgroundColor = "cornsilk";

    if (password.value == "") {
        password.style.backgroundColor = "orange";
        ok = false;
    } else
        password.style.backgroundColor = "cornsilk";

    if (ok)
        return true;

    return false;
}
</script>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Login</title>

        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/style.css" />" />
        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/new_style.css" />" />
        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/animate.css" />" />
    </head>

    <body class = "login">
        <div class = "vertical">
            <div class = "center">
                <h2 class="tlt" data-in-effect="lightSpeedIn">
                    Please Log In
                </h2>

                <table style = "width : 10%">
                        <form id="loginForm" method="post" onsubmit="return checkInfo()">
                            <tr>
                                <td class="tlt" data-in-effect="lightSpeedIn">
                                    <label for = "username" class = "login_label" >Username</label>
                                </td>
                                <td>
                                    <input id="username" class = "login_input" type = "text" name="username" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tlt" data-in-effect="lightSpeedIn">
                                    <label for = "password"class = "login_label" >Password</label>
                                </td>
                                <td>
                                    <input id="password" class = "login_input" type = "password" name="password" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br><input type="submit" name = "loginButton" value="Log In" />
                                </td>
                                <td>
                                    <br><input type="submit" name = "signUpButton" value="Sign Up" />
                                </td>
                            </tr>
                        </form>
                </table>
                ${errorMessage}
            </div>
        </div>
    </body>
</html>