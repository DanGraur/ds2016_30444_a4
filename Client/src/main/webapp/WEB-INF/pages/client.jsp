<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<script src="/resources/jquery-3.1.1.min.js"></script>
<script src="/resources/jquery.lettering.js"></script>
<script src="/resources/jquery.textillate.js"></script>
<script src="/resources/jquery.qtip.min.js"></script>

<link type="text/css" rel="stylesheet" href="<c:url value="/resources/animate.css" />" />
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/style.css" />" />
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/new_style.css" />" />
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/jquery.qtip.min.css" />">

<script>

    $(document).ready(function(){
        $(function () {
            $('.tlt').textillate();
        });
    });

    $(document).ready(function() {

        $.each($('.rowTooltipClass'), function() {
            $currentLink = $(this);

            $currentLink.qtip({
                content: {
                    text: function(event, api) {
                        return $.ajax({
                            url: 'client/status',
                            method: 'GET',
                            data: {packageId: this.attr('data-id')},
                            dataType: 'text'
                        })
                            .then(function(content) {
                                return content
                            }, function(xhr, status, error) {
                                api.set('content.text', status + ': ' + error);
                            });
                    }
                },
                position: {
                    at: 'bottom left',
                    my: 'top right'
                },
                style: {
                    classes: 'qtip-tipsy',
                    width: 500
                }
            });

        });

    });

</script>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>
        User Menu
    </title>


</head>
<body class = "userBody">

<div class = "centerAlignDiv">
    <div>

        <h2 class = "tlt" data-in-effect = "fadeInUp">
            User Packages
        </h2>


        <form id = "packageForm" name = "packageForm" method = "post">

            <input type="hidden" id = "hiddenId" name="hiddenId"/>

            <input class = "searchBarStyle" type = "text" id = "searchBar" name = "searchBar"/>

            <select id = "searchType" name = "searchType" class = "searchDropdown">
                <option value = "name" selected>Name</option>
                <option value = "all">All</option>
            </select>

            <input type = "submit" name = "searchButton" id = "searchButton" value = "Search" />

            <table class = "w3-table-all">
                <tr class = "w3-red">
                    <th>Package Number</th>
                    <th>Sender</th>
                    <th>Receiver</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Start City</th>
                    <th>Destination City</th>
                    <th>Tracked</th>
                </tr>

                <tfoot>
                <tr>
                    <td>
                        <label for = "logOutButton">
                            <c:out value="Log Out: "/>
                        </label>
                    </td>
                    <td>
                        <input type = "submit" id = "logOutButton" name = "logOutButton" value = "Log Out" />
                    </td>

                    <!-- Add dummy elements to make the table visually pleasing START -->

                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <!-- Add dummy elements to make the table visually pleasing END -->

                </tr>
                </tfoot>

                <c:if test="${not empty packages}">
                    <c:forEach var="o" items="${packages}">
                        <tr>
                            <td>
                                <c:out value = "${o.id}"/>
                            </td>
                            <td>
                                <c:forEach var="c" items="${users}">
                                    <c:if test="${c.id == o.suid}">
                                        <c:out value = "${c.username}"/>
                                    </c:if>
                                </c:forEach>
                            </td>
                            <td>
                                <c:forEach var="c" items="${users}">
                                    <c:if test="${c.id == o.duid}">
                                        <c:out value = "${c.username}"/>
                                    </c:if>
                                </c:forEach>
                            </td>
                            <td>
                                <c:out value = "${o.name}"/>
                            </td>
                            <td>
                                <c:out value = "${o.description}"/>
                            </td>
                            <td>
                                <c:forEach var="c" items="${cities}">
                                    <c:if test="${c.id == o.scid}">
                                        <c:out value = "${c.name}"/>
                                    </c:if>
                                </c:forEach>
                            </td>
                            <td>
                                <c:forEach var="c" items="${cities}">
                                    <c:if test="${c.id == o.dcid}">
                                        <c:out value = "${c.name}"/>
                                    </c:if>
                                </c:forEach>
                            </td>
                            <td class = "rowTooltipClass" data-id="${o.id}">
                                <c:choose>
                                    <c:when test="${o.track == 0}">
                                        <c:out value="Not Tracked" />
                                    </c:when>
                                    <c:otherwise>
                                        <c:out value="Tracked" />
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                    </c:forEach>
                </c:if>
            </table>
        </form>
    </div>
</div>
</body>
</html>