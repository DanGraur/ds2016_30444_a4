<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<script src="/resources/jquery-3.1.1.min.js"></script>
<script src="/resources/jquery.lettering.js"></script>
<script src="/resources/jquery.textillate.js"></script>
<script src="/resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

<link type="text/css" rel="stylesheet" href="<c:url value="/resources/animate.css" />" />
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/style.css" />" />
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/new_style.css" />" />
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/jquery-ui-1.12.1.custom/jquery-ui.css" />" >

<script>
    var dialog;

    $(document).ready(function(){
        $(function () {
            $('.tlt').textillate();
        });
    });

    function associatePackageId(id) {

        document.forms["packageForm"].hiddenId.value = id;
        document.forms["packageForm"].submit();

    }
    
    function addPackage() {
        var ok = true;

        var name = document.getElementById("newPackageName");
        var description = document.getElementById("newPackageDescription");

        if (name.value == "") {
            name.style.backgroundColor = "orange";
            ok = false;
        } else
            name.style.backgroundColor = "cornsilk";

        if (description.value == "") {
            description.style.backgroundColor = "orange";
            ok = false;
        } else
            description.style.backgroundColor = "cornsilk";

        return ok;
    }


    function addNewRouteFunction(id) {

        $("#newRoutePackageId").val(id);
        dialog.dialog("open");

    }

    function newRouteCancelFunction() {

        dialog.dialog("close");

    }

    <!-- Add logic for the route dialog -->
    $( function() {

        dialog = $( "#dialog-form" ).dialog({
            autoOpen: false,
            height: 300,
            width: 350,
            modal: true,
        });

    } );


</script>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>
        Admin Menu
    </title>


</head>
<body class = "adminBody">

    <div id="dialog-form" title = "Add New Route Element">
        <form id = "routeForm" name = "routeForm" method = "post">
                <label for="newRoutePackageId">Package Id</label>
                <input type="text" name="newRoutePackageId" id = "newRoutePackageId" readonly = "readonly"/>

                <label for="newRouteCity">City</label>
                <select name = "newRouteCity" id = "newRouteCity">
                    <c:forEach var="o" items="${cities}">
                        <option value = "${o.id}">${o.name}</option>
                    </c:forEach>
                </select>

                <label for="newRouteTime">Time</label>
                <input type="datetime-local" name="newRouteTime" id = "newRouteTime"/>


                <input type="submit" value = "Add Route" name = "newRouteButton">
                <input type="button" value = "Cancel" onclick="newRouteCancelFunction()">
        </form>
    </div>

    <div class = "centerAlignDiv" >
        <div>

            <h2 class = "tlt" data-in-effect = "fadeInUp">
                Packages
            </h2>


            <form id = "packageForm" name = "packageForm" method = "post">

                <input type="hidden" id = "hiddenId" name="hiddenId"/>

                    <table class = "w3-table-all">
                        <tr class = "w3-red">
                            <th>Delete</th>
                            <th>Package Number</th>
                            <th>Sender</th>
                            <th>Receiver</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Start City</th>
                            <th>Destination City</th>
                            <th>Action</th>

                            <!-- Add dummy elements to make the table visually pleasing START -->


                            <!-- Add dummy elements to make the table visually pleasing END -->

                        </tr>

                        <tfoot>
                            <tr>
                                <td>
                                    <label for = "logOutButton">
                                        <c:out value="Log Out: "/>
                                    </label>
                                </td>
                                <td>
                                    <input type = "submit" id = "logOutButton" name = "logOutButton" value = "Log Out" />
                                </td>

                                <!-- Add dummy elements to make the table visually pleasing START -->

                                <td>
                                    <c:out value="${statusMessage}" />
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                                <!-- Add dummy elements to make the table visually pleasing END -->

                            </tr>
                        </tfoot>

                        <tr>
                            <td></td>

                            <td>
                                <c:out value="auto-generated"/>
                            </td>
                            <td>
                                <select name = "newPackageSUid">
                                    <c:forEach var="o" items="${users}">
                                        <option value = "${o.id}">${o.username}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>
                                <select name = "newPackageDUid">
                                    <c:forEach var="o" items="${users}">
                                        <option value = "${o.id}">${o.username}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>
                                <input type = "text" name = "newPackageName" id = "newPackageName">
                            </td>
                            <td>
                                <input type = "text" name = "newPackageDescription" id = "newPackageDescription">
                            </td>
                            <td>
                                <select name = "newPackageSCid">
                                    <c:forEach var="o" items="${cities}">
                                        <option value = "${o.id}">${o.name}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>
                                <select name = "newPackageDCid">
                                    <c:forEach var="o" items="${cities}">
                                        <option value = "${o.id}">${o.name}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>
                                <input type="submit" value="Add" name = "addPackageButton" onclick="return addPackage()" >
                            </td>
                        </tr>
                        <c:if test="${not empty packages}">
                            <c:forEach var="o" items="${packages}">
                                <tr>
                                    <td>
                                        <input type = "submit" name = "deletePackageButton" id = "deletePackageButton" value="X" onclick="associatePackageId(${o.id})">
                                    </td>
                                    <td>
                                        <c:out value = "${o.id}"/>
                                    </td>
                                    <td>
                                        <c:forEach var="c" items="${users}">
                                            <c:if test="${c.id == o.suid}">
                                                <c:out value = "${c.username}"/>
                                            </c:if>
                                        </c:forEach>
                                    </td>
                                    <td>
                                        <c:forEach var="c" items="${users}">
                                            <c:if test="${c.id == o.duid}">
                                                <c:out value = "${c.username}"/>
                                            </c:if>
                                        </c:forEach>
                                    </td>
                                    <td>
                                        <c:out value="${o.name}" />
                                    </td>
                                    <td>
                                        <c:out value="${o.description}" />
                                    </td>
                                    <td>
                                        <c:forEach var="c" items="${cities}">
                                            <c:if test="${c.id == o.scid}">
                                                <c:out value = "${c.name}"/>
                                            </c:if>
                                        </c:forEach>
                                    </td>
                                    <td>
                                        <c:forEach var="c" items="${cities}">
                                            <c:if test="${c.id == o.dcid}">
                                                <c:out value = "${c.name}"/>
                                            </c:if>
                                        </c:forEach>
                                    </td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${o.track == 0}">
                                                <input type="submit" value="Track" name="trackButton" onclick="associatePackageId(${o.id})"/>
                                            </c:when>
                                            <c:otherwise>
                                                <input type="button" value="Add Route" name="addNewRoute" onclick="addNewRouteFunction(${o.id})"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                            </c:forEach>
                         </c:if>
                    </table>
            </form>
        </div>
    </div>
</body>
</html>