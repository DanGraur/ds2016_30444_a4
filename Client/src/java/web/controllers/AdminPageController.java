package web.controllers;

import admin.Package;
import admin.PackageOperations;
import admin.PackageOperationsImplService;
import auxiliary.AuxiliaryOperations;
import auxiliary.AuxiliaryOperationsImplService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import utility.AdminObjectFactory;
import utility.ClientObjectFactory;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by noi on 12/12/2016.
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminPageController {

    @RequestMapping(method = RequestMethod.GET)
    public String displayAdminPage(Model model) {
        AuxiliaryOperationsImplService auxService = new AuxiliaryOperationsImplService();
        AuxiliaryOperations auxiliaryOperations = auxService.getAuxiliaryOperationsImplPort();

        PackageOperationsImplService packageService = new PackageOperationsImplService();
        PackageOperations packageOperations = packageService.getPackageOperationsImplPort();

        // Add the cities
        model.addAttribute("cities", auxiliaryOperations.getAllCities().getItem());
        // Add the users
        model.addAttribute("users", auxiliaryOperations.getAllUsers().getItem());
        // Add the packages
        model.addAttribute("packages", packageOperations.getAllPackages().getItem());

        return "admin";
    }

    @RequestMapping(method = RequestMethod.POST, params = "addPackageButton")
    public String addPackage(@RequestParam(value = "newPackageSUid") int suid,
                             @RequestParam(value = "newPackageDUid") int duid,
                             @RequestParam(value = "newPackageName") String name,
                             @RequestParam(value = "newPackageDescription") String description,
                             @RequestParam(value = "newPackageSCid") int scid,
                             @RequestParam(value = "newPackageDCid") int dcid,
                             Model model) {

        // Create the new object
        Package pcg = AdminObjectFactory.createPackage(suid, duid, name, description, scid, dcid);

        // Add the new object
        PackageOperationsImplService packageService = new PackageOperationsImplService();
        PackageOperations packageOperations = packageService.getPackageOperationsImplPort();

        if (name.isEmpty() || description.isEmpty())
            model.addAttribute("statusMessage", "Invalid Name or Date");
        else
            packageOperations.addPackage(pcg);

        // Update the current data
        AuxiliaryOperationsImplService auxService = new AuxiliaryOperationsImplService();
        AuxiliaryOperations auxiliaryOperations = auxService.getAuxiliaryOperationsImplPort();

        // Add the cities
        model.addAttribute("cities", auxiliaryOperations.getAllCities().getItem());
        // Add the users
        model.addAttribute("users", auxiliaryOperations.getAllUsers().getItem());
        // Add the packages
        model.addAttribute("packages", packageOperations.getAllPackages().getItem());

        return "admin";
    }


    @RequestMapping(method = RequestMethod.POST, params = "deletePackageButton")
    public String deletePackage(@RequestParam(value = "hiddenId") int id,
                                Model model) {
        PackageOperationsImplService packageService = new PackageOperationsImplService();
        PackageOperations packageOperations = packageService.getPackageOperationsImplPort();

        packageOperations.removePackage(id);

        // Update the current data
        AuxiliaryOperationsImplService auxService = new AuxiliaryOperationsImplService();
        AuxiliaryOperations auxiliaryOperations = auxService.getAuxiliaryOperationsImplPort();

        // Add the cities
        model.addAttribute("cities", auxiliaryOperations.getAllCities().getItem());
        // Add the users
        model.addAttribute("users", auxiliaryOperations.getAllUsers().getItem());
        // Add the packages
        model.addAttribute("packages", packageOperations.getAllPackages().getItem());

        return "admin";
    }


    @RequestMapping(method = RequestMethod.POST, params = "trackButton")
    public String trackPackage(@RequestParam(value = "hiddenId") int id,
                               Model model) {
        PackageOperationsImplService packageService = new PackageOperationsImplService();
        PackageOperations packageOperations = packageService.getPackageOperationsImplPort();

        packageOperations.enableTracking(id);

        // Update the current data
        AuxiliaryOperationsImplService auxService = new AuxiliaryOperationsImplService();
        AuxiliaryOperations auxiliaryOperations = auxService.getAuxiliaryOperationsImplPort();

        // Add the cities
        model.addAttribute("cities", auxiliaryOperations.getAllCities().getItem());
        // Add the users
        model.addAttribute("users", auxiliaryOperations.getAllUsers().getItem());
        // Add the packages
        model.addAttribute("packages", packageOperations.getAllPackages().getItem());

        return "admin";
    }


    @RequestMapping(method = RequestMethod.POST, params = "newRouteButton")
    public String addNewRoute(@RequestParam(value = "newRoutePackageId") int id,
                              @RequestParam(value = "newRouteCity") int cid,
                              @RequestParam(value = "newRouteTime") String dateTime,
                              Model model) {

        AuxiliaryOperationsImplService auxService = new AuxiliaryOperationsImplService();
        AuxiliaryOperations auxiliaryOperations = auxService.getAuxiliaryOperationsImplPort();

        PackageOperationsImplService packageService = new PackageOperationsImplService();
        PackageOperations packageOperations = packageService.getPackageOperationsImplPort();

        // Add the route element

        //System.out.println(dateTime);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");

        try {

            packageOperations.addRouteElement(AdminObjectFactory.createRoute(id, cid, dateFormat.parse(dateTime).getTime()));

        } catch (ParseException e) {
            model.addAttribute("statusMessage", "Invalid Date");

            e.printStackTrace();
        }

        // Add the cities
        model.addAttribute("cities", auxiliaryOperations.getAllCities().getItem());
        // Add the users
        model.addAttribute("users", auxiliaryOperations.getAllUsers().getItem());
        // Add the packages
        model.addAttribute("packages", packageOperations.getAllPackages().getItem());

        return "admin";
    }

    @RequestMapping(method = RequestMethod.POST, params = "logOutButton")
    public String logout(HttpServletRequest request) {
        request.getSession().invalidate();

        return "redirect:login";
    }

}
