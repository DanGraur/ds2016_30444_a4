package web.controllers;

import client.User;
import client.UserOperationsService;
import client.UserOperationsServiceSoap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import utility.ClientObjectFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by noi on 12/10/2016.
 */
@Controller
@RequestMapping(value = "/login")
public class LoginController {

    @RequestMapping(method = RequestMethod.GET)
    public String displayLogin() {
        return "login";
    }

    private boolean checkCredentialsForValidity(String username, String password) {
        if (username.equals("") || password.equals(""))
            return false;

        return true;
    }

    @RequestMapping(method = RequestMethod.POST, params = "loginButton")
    public String login(@RequestParam(value = "username", required = false) String username,
                        @RequestParam(value = "password", required = false) String password,
                        HttpServletRequest request,
                        Model model) {

        // Check the validity of the credentials
        if (!checkCredentialsForValidity(username, password)) {
            model.addAttribute("errorMessage", "Cannot have an invalid username or password");

            return "login";
        }

        HttpSession userSession = request.getSession();

        UserOperationsServiceSoap userOperations = new UserOperationsService().getUserOperationsServiceSoap();

        User user = userOperations.getUser(username, password);

        if (user != null) {
            userSession.setAttribute("id", user.getId());

            if (user.getType().equals("admin")) {
                userSession.setAttribute("type", "admin");

                return "redirect:admin";
            } else {
                userSession.setAttribute("type", "regular");

                return "redirect:client";
            }
        }

        model.addAttribute("errorMessage", "Failed to Login");
        return "login";
    }

    @RequestMapping(method = RequestMethod.POST, params = "signUpButton")
    public String signUp(@RequestParam(value = "username") String username,
                         @RequestParam(value = "password") String password,
                         HttpServletRequest request,
                         Model model) {

        // Check the validity of the credentials
        if (!checkCredentialsForValidity(username, password)) {
            model.addAttribute("errorMessage", "Cannot have an invalid username or password");

            return "login";
        }

        HttpSession userSession = request.getSession();

        UserOperationsServiceSoap userOperations = new UserOperationsService().getUserOperationsServiceSoap();

        boolean exists = userOperations.checkIfUserExists(username);

        if (!exists) {
            int id = userOperations.addUser(ClientObjectFactory.createUser(username, password, "regular"));

            userSession.setAttribute("type", "regular");
            userSession.setAttribute("id", id);
            return "redirect:client";
        }

        model.addAttribute("errorMessage", "User already exists");
        return "login";
    }

}
