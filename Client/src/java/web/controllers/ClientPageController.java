package web.controllers;

import admin.PackageOperations;
import admin.PackageOperationsImplService;
import auxiliary.AuxiliaryOperations;
import auxiliary.AuxiliaryOperationsImplService;
import auxiliary.City;
import client.Route;
import client.UserOperationsService;
import client.UserOperationsServiceSoap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by noi on 12/12/2016.
 */
@Controller
@RequestMapping(value = "/client")
@SessionAttributes("id")
public class ClientPageController {

    @RequestMapping(method = RequestMethod.GET)
    public String displayUserPage(@ModelAttribute(value = "id") int id,
                                  Model model) {
        UserOperationsService userService = new UserOperationsService();
        UserOperationsServiceSoap userOperations = userService.getUserOperationsServiceSoap();

        AuxiliaryOperationsImplService auxService = new AuxiliaryOperationsImplService();
        AuxiliaryOperations auxiliaryOperations = auxService.getAuxiliaryOperationsImplPort();

        // Add the cities
        model.addAttribute("cities", auxiliaryOperations.getAllCities().getItem());
        // Add the users
        model.addAttribute("users", auxiliaryOperations.getAllUsers().getItem());
        // Add the packages
        model.addAttribute("packages", userOperations.getUserPackages(id).getPackage());

        return "client";
    }


    @RequestMapping(method =  RequestMethod.POST, params = "searchButton")
    public String performSearch(@ModelAttribute(value = "id") int id,
                                @RequestParam(value = "searchType") String searchType,
                                @RequestParam(value = "searchBar", required = false) String searchString,
                                Model model) {
        UserOperationsService userService = new UserOperationsService();
        UserOperationsServiceSoap userOperations = userService.getUserOperationsServiceSoap();

        AuxiliaryOperationsImplService auxService = new AuxiliaryOperationsImplService();
        AuxiliaryOperations auxiliaryOperations = auxService.getAuxiliaryOperationsImplPort();

        // Add the cities
        model.addAttribute("cities", auxiliaryOperations.getAllCities().getItem());
        // Add the users
        model.addAttribute("users", auxiliaryOperations.getAllUsers().getItem());
        // Add the packages

        if (searchType.equals("all"))
            model.addAttribute("packages", userOperations.getUserPackages(id).getPackage());
        else
            model.addAttribute("packages", userOperations.searchUserPackages(id, searchString).getPackage());

        return "client";
    }

    @ResponseBody
    @RequestMapping(value = "status", method = RequestMethod.GET)
    public String getPackageStatus(@RequestParam("packageId") String stringId) {
        int id = Integer.parseInt(stringId);

        AuxiliaryOperationsImplService auxService = new AuxiliaryOperationsImplService();
        AuxiliaryOperations auxiliaryOperations = auxService.getAuxiliaryOperationsImplPort();

        UserOperationsService userService = new UserOperationsService();
        UserOperationsServiceSoap userOperations = userService.getUserOperationsServiceSoap();

        // Get the cities
        Map<Integer, String> cityMap = new HashMap<Integer, String>();

        for (City c : auxiliaryOperations.getAllCities().getItem())
            cityMap.put(c.getId(), c.getName());

        // Get the route sorted by time
        List<Route> route = userOperations.getPackageRoute(id).getRoute();

        if (route.isEmpty())
            return "Currently not tracked";

        // Generate the status text, based on the current date
        SimpleDateFormat dateFormat = new SimpleDateFormat("'('dd-MM-yyyy' 'hh:mm')'");
        long currentTime = System.currentTimeMillis();
        StringBuilder statusText = new StringBuilder();
        boolean passedCurrent = false;

        Iterator<Route> iterator = route.iterator();

        Route current = iterator.next();
        Route aux;

        while(iterator.hasNext()) {
            aux = current;
            current = iterator.next();

            String auxDate = dateFormat.format(new Date(aux.getTime()));

            if (!passedCurrent && aux.getTime() <= currentTime && current.getTime() > currentTime) {
                passedCurrent = true;

                statusText.append("<span style = 'color: red'>" + cityMap.get(aux.getCid()) + ' ' + auxDate +  "</span>" + " --> ");
            } else
                statusText.append(cityMap.get(aux.getCid()) + ' ' + auxDate + " --> ");
        }

        String currentDate = dateFormat.format(new Date(current.getTime()));

        if (!passedCurrent)
            statusText.append("<span style = 'color: red'>" + cityMap.get(current.getCid()) + ' ' + currentDate + "</span>" + " --> " );
        else
            statusText.append(cityMap.get(current.getCid()) + ' ' + currentDate + " --> ");

        return statusText.append(" (End) ").toString();
    }

    @RequestMapping(method =  RequestMethod.POST, params = "logOutButton")
    public String logout(HttpServletRequest request) {
        request.getSession().invalidate();

        return "redirect:login";
    }

}
