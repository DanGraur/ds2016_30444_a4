package web.interceptors;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import web.utility.SessionUtilities;


/**
 * Created by noi on 12/10/2016.
 */
public class AdminInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(javax.servlet.http.HttpServletRequest httpServletRequest,
                             javax.servlet.http.HttpServletResponse httpServletResponse,
                             Object o) throws Exception {

        String type = (String) httpServletRequest.getSession().getAttribute("type");

        if (type == null || !type.equals("admin")) {
            if (type != null && type.equals("regular"))
                httpServletResponse.sendRedirect("client");
            else
                httpServletResponse.sendRedirect("login");

            return false;
        }

        return true;
    }

    @Override
    public void postHandle(javax.servlet.http.HttpServletRequest httpServletRequest,
                           javax.servlet.http.HttpServletResponse httpServletResponse,
                           Object o,
                           ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(javax.servlet.http.HttpServletRequest httpServletRequest,
                                javax.servlet.http.HttpServletResponse httpServletResponse,
                                Object o,
                                Exception e) throws Exception {

    }
}
