package utility;

import admin.Package;
import admin.Route;

/**
 * Created by noi on 12/10/2016.
 */
public class AdminObjectFactory {

    public static Package createPackage(int suid, int duid, String name, String description, int scid, int dcid) {
        Package pcg = new Package();

        pcg.setId(0);
        pcg.setSuid(suid);
        pcg.setDuid(duid);
        pcg.setName(name);
        pcg.setDescription(description);
        pcg.setScid(scid);
        pcg.setDcid(dcid);
        pcg.setTrack(0);

        return pcg;
    }

    public static Route createRoute(int pid, int cid, long time) {
        Route route = new Route();

        route.setPid(pid);
        route.setCid(cid);
        route.setTime(time);

        return route;
    }
}
