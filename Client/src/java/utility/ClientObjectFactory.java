package utility;

import client.User;

/**
 * Created by noi on 12/12/2016.
 */
public class ClientObjectFactory {

    public static User createUser(String username, String password, String type) {
        User user = new User();

        user.setUsername(username);
        user.setPassword(password);
        user.setType(type);

        return user;
    }
}
