package logic;

import admin.Package;

import admin.PackageOperations;
import admin.PackageOperationsImplService;
import admin.Route;
import client.UserOperationsService;
import client.UserOperationsServiceSoap;
import utility.AdminObjectFactory;

import java.util.List;
import java.util.Random;

/**
 * Created by noi on 12/9/2016.
 */
public class Test {

    public static void main(String[] args) {

        /*
        PackageOperationsImplService adminService = new PackageOperationsImplService();

        PackageOperations adminOperations = adminService.getPackageOperationsImplPort();
        */

        /*
        // Create package
        Package pcg = AdminObjectFactory.createPackage(1, 1, "Song", "Description 3", 1, 4);
        adminOperations.addPackage(pcg);

        // Enable tracking
        //Package retPackage = adminOperations.getPackage(3);
        adminOperations.enableTracking(3);

        // Add route element
        Route routeElement = AdminObjectFactory.createRoute(3, 2, (new Random()).nextLong());
        adminOperations.addRouteElement(routeElement);

        */

        /*
        adminOperations.removePackage(3);

        // List all packages
        List<Package> packages = adminOperations.getAllPackages().getItem();

        for (Package p : packages)
            System.out.println(p.getDescription());
        */

        UserOperationsService userService = new UserOperationsService();
        UserOperationsServiceSoap userOperations = userService.getUserOperationsServiceSoap();

        List<client.Route> route = userOperations.getPackageRoute(1).getRoute();

        for (client.Route r : route)
            System.out.println(r.getId() + " " + r.getPid() + " " + r.getCid() + " " + r.getTime());

    }
}
