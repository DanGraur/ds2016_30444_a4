package data.pcg;

import data.entity.Package;

import java.util.List;


/**
 * Created by noi on 12/8/2016.
 */
public interface PackageDAO {
    Package getPackage(int id);

    Package getPackage(String name);

    List<Package> getAllPackages();

    void addPackage(Package pcg);

    void removePackage(int id);

    void updatePackage(Package pcg);
}
