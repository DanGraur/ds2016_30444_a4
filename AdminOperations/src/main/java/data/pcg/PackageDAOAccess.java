package data.pcg;

import data.entity.Package;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;


public class PackageDAOAccess implements PackageDAO {

    private SessionFactory factory;

    public PackageDAOAccess(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public Package getPackage(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Package pcg = null;

        try {
            tx = session.beginTransaction();

            pcg = (Package) session
                    .createQuery("FROM Package WHERE id = :id")
                    .setParameter("id", id)
                    .getSingleResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return pcg;
    }

    @Override
    public Package getPackage(String name) {
        Session session = factory.openSession();
        Transaction tx = null;
        Package pcg = null;

        try {
            tx = session.beginTransaction();

            pcg = (Package) session
                    .createQuery("FROM Package WHERE name = :name")
                    .setParameter("name", name)
                    .getSingleResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return pcg;
    }

    @Override
    public List<Package> getAllPackages() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Package> list = null;

        try {
            tx = session.beginTransaction();

            list = session.createQuery("FROM Package").list();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return list;
    }

    @Override
    public void addPackage(Package pcg) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            session.save(pcg);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void removePackage(int id) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            session
                    .createQuery("DELETE FROM Package where id = :id")
                    .setParameter("id", id)
                    .executeUpdate();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void updatePackage(Package pcg) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            session.update(pcg);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }
    }


}
