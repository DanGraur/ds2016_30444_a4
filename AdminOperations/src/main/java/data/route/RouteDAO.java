package data.route;

import data.entity.Route;

import java.util.List;

/**
 * Created by noi on 12/8/2016.
 */
public interface RouteDAO {
    Route getRoute(int id);

    void addRoute(Route route);

    void addRoutes(List<Route> routes);

    void updateRoutes(List<Route> routes);

    List<Route>  getAllRoutes();
}
