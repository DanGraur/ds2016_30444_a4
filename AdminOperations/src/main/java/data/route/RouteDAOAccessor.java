package data.route;

import data.entity.Route;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;


public class RouteDAOAccessor implements RouteDAO {


    private SessionFactory factory;

    public RouteDAOAccessor(SessionFactory factory) {
        this.factory = factory;
    }



    @Override
    public Route getRoute(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Route route = null;

        try {
            tx = session.beginTransaction();

            route = session.get(Route.class, id);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return route;
    }

    @Override
    public void addRoute(Route route) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            session.save(route);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void addRoutes(List<Route> routes) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            for (Route r : routes)
                session.save(r);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void updateRoutes(List<Route> routes) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            for (Route r : routes)
                session.update(r);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public List<Route> getAllRoutes() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Route> list = null;

        try {
            tx = session.beginTransaction();

            list = session.createQuery("FROM Route").list();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return list;
    }
}
