package data.entity;

/**
 * Created by noi on 12/8/2016.
 */
public class Package {
    private int id;
    private int suid;
    private int duid;
    private String name;
    private String description;
    private int scid;
    private int dcid;
    private int track;

    public Package() {
    }

    public Package(int suid, int duid, String name, String description, int scid, int dcid, int track) {
        this.suid = suid;
        this.duid = duid;
        this.name = name;
        this.description = description;
        this.scid = scid;
        this.dcid = dcid;
        this.track = track;
    }

    public Package(int id, int suid, int duid, String name, String description, int scid, int dcid, int track) {
        this.id = id;
        this.suid = suid;
        this.duid = duid;
        this.name = name;
        this.description = description;
        this.scid = scid;
        this.dcid = dcid;
        this.track = track;
    }

    @Override
    public String toString() {
        return "Package{" +
                "id=" + id +
                ", suid=" + suid +
                ", duid=" + duid +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", scid=" + scid +
                ", dcid=" + dcid +
                ", track=" + track +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSuid() {
        return suid;
    }

    public void setSuid(int suid) {
        this.suid = suid;
    }

    public int getDuid() {
        return duid;
    }

    public void setDuid(int duid) {
        this.duid = duid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getScid() {
        return scid;
    }

    public void setScid(int scid) {
        this.scid = scid;
    }

    public int getDcid() {
        return dcid;
    }

    public void setDcid(int dcid) {
        this.dcid = dcid;
    }

    public int getTrack() {
        return track;
    }

    public void setTrack(int track) {
        this.track = track;
    }
}
