package data.entity;

/**
 * Created by noi on 12/8/2016.
 */

public class Route {
    private int id;
    private int pid;
    private int cid;
    private long time;

    public Route() {
    }

    public Route(int pid, int cid, long time) {
        this.pid = pid;
        this.cid = cid;
        this.time = time;
    }

    public Route(int id, int pid, int cid, long time) {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.time = time;
    }

    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", pid=" + pid +
                ", cid=" + cid +
                ", time=" + time +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
