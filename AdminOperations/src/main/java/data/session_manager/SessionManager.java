package data.session_manager;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionManager {
    private static SessionFactory sessionFactory;

    private SessionManager() {
    }

    /**
     * Returns a reference to the singleton SessionFactory object
     *
     * @return the singleton SessionFactory object
     */
    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null)
            sessionFactory = new Configuration().configure().buildSessionFactory();

        return sessionFactory;
    }

}
