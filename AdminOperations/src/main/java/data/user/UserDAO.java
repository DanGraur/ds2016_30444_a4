package data.user;

import data.entity.User;

import java.util.List;

/**
 * Created by noi on 12/8/2016.
 */
public interface UserDAO {
    User getUser(String username, String password);

    void addUser(User user);

    List<User> getAllUsers();
}
