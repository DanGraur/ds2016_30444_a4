package data.user;

import data.entity.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class UserDAOAccessor implements UserDAO {

    private SessionFactory factory;

    public UserDAOAccessor(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public User getUser(String username, String password) {
        Session session = factory.openSession();
        Transaction tx = null;
        User user = null;

        try {
            tx = session.beginTransaction();

            Query searchQuery = session.createQuery("FROM User WHERE username = :name AND password = :password");

            searchQuery.setParameter("name", username);
            searchQuery.setParameter("password", password);

            user = (User) searchQuery.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return user;
    }

    @Override
    public void addUser(User user) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            session.save(user);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public List<User> getAllUsers() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<User> list = null;

        try {
            tx = session.beginTransaction();

            list = session
                    .createQuery("FROM User")
                    .list();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return list;
    }
}
