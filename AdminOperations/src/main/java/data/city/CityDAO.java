package data.city;


import data.entity.City;

import java.util.List;

/**
 * Created by noi on 12/8/2016.
 */
public interface CityDAO {
    City getCity(int id);

    List<City> getAllCities();

}
