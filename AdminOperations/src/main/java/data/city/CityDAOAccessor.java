package data.city;

import data.entity.City;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;


public class CityDAOAccessor implements CityDAO {

    private SessionFactory factory;

    public CityDAOAccessor(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public City getCity(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        City city = null;

        try {
            tx = session.beginTransaction();

            Query searchQuery = session.createQuery("FROM City WHERE id = :id");

            searchQuery.setParameter("id", id);

            city = (City) searchQuery.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return city;
    }

    @Override
    public List<City> getAllCities() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<City> list = null;

        try {
            tx = session.beginTransaction();

            list = session.createQuery("FROM City").list();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return list;
    }

}
