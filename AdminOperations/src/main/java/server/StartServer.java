package server;

import logic.AuxiliaryOperationsImpl;
import logic.PackageOperationsImpl;

import javax.xml.ws.Endpoint;

/**
 * Created by noi on 12/9/2016.
 */
public class StartServer {

    public static void main(String[] args) {
        // Publish the admin services
        Endpoint.publish("http://localhost:9090/services/admin", new PackageOperationsImpl());

        // Publish the auxiliary services
        Endpoint.publish("http://localhost:9090/services/auxiliary", new AuxiliaryOperationsImpl());
    }

}
