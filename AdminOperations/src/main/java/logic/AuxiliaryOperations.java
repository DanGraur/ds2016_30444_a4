package logic;

import data.entity.City;
import data.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Created by noi on 12/15/2016.
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface AuxiliaryOperations {

    @WebMethod
    City[] getAllCities();

    @WebMethod
    User[] getAllUsers();

}
