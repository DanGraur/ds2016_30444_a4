package logic;

import data.entity.Package;
import data.entity.Route;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Created by noi on 12/9/2016.
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface PackageOperations {

    @WebMethod
    void addPackage(Package pcg);

    @WebMethod
    void removePackage(int id);

    @WebMethod
    void enableTracking(int id);

    @WebMethod
    void addRouteElement(Route route);

    @WebMethod
    Package getPackage(int id);

    @WebMethod
    Package[] getAllPackages();
}
