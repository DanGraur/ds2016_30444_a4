package logic;

import data.entity.Package;
import data.entity.Route;
import data.pcg.PackageDAO;
import data.pcg.PackageDAOAccess;
import data.route.RouteDAO;
import data.route.RouteDAOAccessor;
import data.session_manager.SessionManager;

import javax.jws.WebService;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by noi on 12/9/2016.
 */
@WebService(endpointInterface = "logic.PackageOperations")
public class PackageOperationsImpl implements PackageOperations {

    @Override
    public void removePackage(int id) {
        PackageDAO packageDAO = new PackageDAOAccess(SessionManager.getSessionFactory());

        packageDAO.removePackage(id);
    }

    @Override
    public void addPackage(Package pcg) {
        PackageDAO packageDAO = new PackageDAOAccess(SessionManager.getSessionFactory());

        packageDAO.addPackage(pcg);
    }

    @Override
    public void enableTracking(int id) {
        PackageDAO packageDAO = new PackageDAOAccess(SessionManager.getSessionFactory());
        RouteDAO routeDAO = new RouteDAOAccessor(SessionManager.getSessionFactory());

        Package pcg = packageDAO.getPackage(id);

        pcg.setTrack(1);
        packageDAO.updatePackage(pcg);

        Route startRoute = new Route(id, pcg.getScid(), new Date().getTime());

        // Add end of route @ around 5 - 10 minutes
        Route endRoute = new Route(id, pcg.getDcid(), new Date().getTime() + (new Random()).nextInt(300000) + 300000);

        routeDAO.addRoute(startRoute);
        routeDAO.addRoute(endRoute);
    }

    @Override
    public void addRouteElement(Route route) {
        RouteDAO routeDAO = new RouteDAOAccessor(SessionManager.getSessionFactory());

        routeDAO.addRoute(route);
    }

    @Override
    public Package getPackage(int id) {
        PackageDAO packageDAO = new PackageDAOAccess(SessionManager.getSessionFactory());

        return packageDAO.getPackage(id);
    }

    @Override
    public Package[] getAllPackages() {
        PackageDAO packageDAO = new PackageDAOAccess(SessionManager.getSessionFactory());
        List<Package> packages = packageDAO.getAllPackages();

        return packages.toArray(new Package[packages.size()]);
    }


}
