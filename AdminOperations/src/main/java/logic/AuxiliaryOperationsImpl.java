package logic;

import data.city.CityDAO;
import data.city.CityDAOAccessor;
import data.entity.City;
import data.entity.User;
import data.session_manager.SessionManager;
import data.user.UserDAO;
import data.user.UserDAOAccessor;

import javax.jws.WebService;
import java.util.List;

/**
 * Created by noi on 12/15/2016.
 */
@WebService(endpointInterface = "logic.AuxiliaryOperations")
public class AuxiliaryOperationsImpl implements AuxiliaryOperations {
    @Override
    public City[] getAllCities() {
        CityDAO cityDAO = new CityDAOAccessor(SessionManager.getSessionFactory());
        List<City> cities = cityDAO.getAllCities();

        return cities.toArray(new City[cities.size()]);
    }

    @Override
    public User[] getAllUsers() {
        UserDAO userDAO = new UserDAOAccessor(SessionManager.getSessionFactory());
        List<User> users = userDAO.getAllUsers();

        for (User u : users)
            u.setPassword("");

        return users.toArray(new User[users.size()]);
    }
}
